import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestaurantRequestRoutingModule } from './restaurant-request-routing.module';
import { RestaurantRequestComponent } from './restaurant-request.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [RestaurantRequestComponent],
  imports: [
    CommonModule,
    RestaurantRequestRoutingModule,
    NgxSkeletonLoaderModule,
    NgxSpinnerModule,
    SharedModule
  ]
})
export class RestaurantRequestModule { }
