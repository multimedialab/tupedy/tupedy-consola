import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantRequestComponent } from './restaurant-request.component';


const routes: Routes = [
  {
    path: '',
    component: RestaurantRequestComponent,
    data: {
      breadcrumb: 'Requests'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantRequestRoutingModule { }
